const path = require("path")

module.exports = {
    mode: "development",
    entry: "./src/Program.fs.js",
    output: {
        path: path.resolve(__dirname, "./public"),
        filename: "bundle.js"
    },
    devServer: {
        contentBase: path.resolve(__dirname, "./public"),
        port: 8080
    }
}